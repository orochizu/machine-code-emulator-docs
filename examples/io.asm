# Reads value from input and decrement it in loop
in r0, term
mov r1, r0
mov $0, r1
out r1, term
sub $0, 1
mov r1, $0
jnz 3
