# MCE Docs

## Instructions

| Command | Arguments           | Description                                               | Example            |
| ------- | ------------------- | --------------------------------------------------------- | ------------------ |
| MOV     | destination, source | Move source value to destination                          | `MOV R1, R0`       |
| ADD     | destination, source | Add source value to destination (integer)                 | `ADD R1, 3`        |
| SUB     | destination, source | Subtract source value from destination (integer)          | `SUB R1, 3`        |
| MUL     | destination, source | Multiply destination by source value (integer)            | `MUL R1, 3`        |
| DIV     | destination, source | Divide destination by source value (integer)              | `DIV R1, 3`        |
| FADD    | destination, source | Add source value to destination (float)                   | `FADD R1, 3.14`    |
| FSUB    | destination, source | Subtract source value from destination (float)            | `FSUB R1, 3.14`    |
| FMUL    | destination, source | Multiply destination by source value (float)              | `FMUL R1, 3.14`    |
| FDIV    | destination, source | Divide destination by source value (float)                | `FDIV R1, 3.14`    |
| IN      | destination, PORT   | Read value from input and move to destination             | `IN R0, TERMINAL`  |
| OUT     | source, PORT        | Read value from source and print in terminal              | `OUT R1, TERMINAL` |
| JMP     | line_number         | Jump to line                                              | `JMP 3`            |
| JZ      | line_number         | Jump to line if last operation result as 0                | `JZ 3`             |
| JNZ     | line_number         | Jump to line if last operation didn't result as 0         | `JNZ 3`            |
| JG      | line_number         | Jump to line if last operation result was greater as 0    | `JG 3`             |
| JNG     | line_number         | Jump to line if last operation result wasn't greater as 0 | `JNG 3`            |
| JL      | line_number         | Jump to line if last operation result was less as 0       | `JL 3`             |
| JNL     | line_number         | Jump to line if last operation result wasn't greater as 0 | `JNL 3`            |

## Addressing

### Immediate operand

Reading values from memory cell. For example: `MOV $0, 100` will move value 100 to first cell.

### Register operand

Reading values from a registry. For example: `FADD R1, 3.14` will add value 3.14 to second cell.

### Memory operand

Reading values from memory cell pointed by value in a registry. For example:

```asm
MOV R0, 5
MOV %R0, R0
```

Above example will move value 5 to first register and in second line move this value to sixth memory cell.
